#include<stdio.h>
int input_row()
{
    int row;
    printf("Enter the number of rows in the matrix-");
    scanf("%d",&row);
    return row;
}
int input_column()
{
    int col;
    printf("Enter the number of columns in the matrix-");
    scanf("%d",&col);
    return col;
}
void input_matrix(int row,int col,int a[row][col])
{
    printf("Enter the matrix elements-\n");
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<col;j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
}
int output_matrix(int row,int col,int a[row][col])
{
    printf("The given matrix=\n");
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<col;j++)
        {
            printf("%d\t",a[i][j]);
        }
        printf("\n");
    }
    return 0;
    
}
int find_transpose(int row,int col,int a[row][col],int t[col][row])
{
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<col;j++)
        {
            t[j][i]=a[i][j];
        }
    }
    return 0;
}
int output_transpose(int row,int col,int t[col][row])
{
    printf("Its transpose=\n");
    for(int i=0;i<col;i++)
    {
        for(int j=0;j<row;j++)
        {
            printf("%d\t",t[i][j]);
        }
        printf("\n");
    }
    return 0;
}
int main()
{
    int row,col;
    row=input_row();
    col=input_column();
    int a[row][col],t[col][row];
    input_matrix(row,col,a);
    output_matrix(row,col,a);
    find_transpose(row,col,a,t);
    output_transpose(row,col,t);
    return 0;
}