#include<stdio.h>
int input()
{
    int n;
    printf("Enter the limit-");
    scanf("%d",&n);
    return n;
}
int compute(int n)
{
    int sum=0;
    for(int i=1;i<=n;i++)
    {
        sum=sum+(i*i);
    }
    return sum;
}
int output(int sum)
{
    printf("Sum=%d",sum);
    return 0;
}
int main()
{
    int n,sum;
    n=input();
    sum=compute(n);
    output(sum);
    return 0;
}    