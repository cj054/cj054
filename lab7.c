#include<stdio.h>
int input()
{
	int n;
	printf("Enter the number of array elements-");
	scanf("%d",&n);
	return n;
}
void input_array(int n,int a[n])
{
	int i;
	printf("Enter the elements-");
	for(i=0;i<n;i++)
	scanf("%d",&a[i]);
}
int find_sum(int n,int a[n])
{
	int sum=0;
	for(int i=0;i<n;i++)
	sum=sum+a[i];
	return sum;
}
float find_avg(int sum,int n)
{
	float avg=(float)sum/n;
	return avg;
}
int output(float avg)
{
	printf("Average of the elements=%f\n",avg);
	return 0;
}	
int main()
{
	int n,sum;
	float avg;
	n=input();
	int a[n];
	input_array(n,a);
	sum=find_sum(n,a);
	avg=find_avg(sum,n);
	output(avg);
	return 0;
}	