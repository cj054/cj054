#include<stdio.h>
#include<string.h>
struct students
{
    int roll;
    char name[20];
    char sec;
    char dep[10];
    int fees;
    float results;
};
typedef struct students stud;
void input(int *n)
{
    printf("Enter the number of students-");
    scanf("%d",n);
}
stud input_student()
{
    stud s1;
    printf("Roll:");
    scanf("%d",&s1.roll);
    printf("Name:");
    gets(s1.name);
    printf("Section:");
    scanf("%c",&s1.sec);
    printf("Department:");
    gets(s1.dep);
    printf("Fees:");
    scanf("%d",&s1.fees);
    printf("Result/Marks:");
    scanf("%f",&s1.results);
    printf("\n");
    return s1;
}
void input_n_students(int n,stud s[n])
{
    for(int i=0;i<n;i++)
    {
        printf("Student %d-\n",i+1);
        s[i]=input_student();
    }
}
stud check_high(int n,stud s[n])
{
    stud high;
    for(int i=0;i<n-1;i++)
    {
        if(s[i].results>s[i+1].results)
        {
        high.roll=s[i].roll;
        strcpy(high.name,s[i].name);
        high.sec=s[i].sec;
        strcpy(high.dep,s[i].dep);
        high.fees=s[i].fees;
        high.results=s[i].results;
        }
    }
    return high;
}
void output(stud high)
{
    printf("The student details with the highest marks-\n");
    printf("Roll:");
    printf("%d",high.roll);
    printf("Name:");
    puts(high.name);
    printf("Section:");
    printf("%c",high.sec);
    printf("Department:");
    puts(high.dep);
    printf("Fees:");
    printf("%d",high.fees);
    printf("Result/Marks:");
    printf("%f\n",high.results);
}
int main()
{
    int n;
    input(&n);
    stud s[n],high;
    input_n_students(n,s);
    high=check_high(n,s);
    output(high);
    return 0;
}