#include<stdio.h>
#include<limits.h>
int input()
{
	int n;
	printf("Enter the number of elements-");
	scanf("%d",&n);
	return n;
}
void input_array(int n,int a[n])
{
	printf("Enter the elements-");
	for(int i=0;i<n;i++)
	scanf("%d",&a[i]);
}
int find_small(int n,int a[n])
{
	int small=INT_MAX;
	for(int i=0;i<n;i++)
	{
		if(a[i]<small)
		small=i;
	}
	return small;
}
int find_large(int n,int a[n])
{
	int large=INT_MIN;
	for(int i=0;i<n;i++)
	{
		if(a[i]>large)
		large=i;
	}
	return large;
}
int swap(int large,int small,int n,int a[n])
{
	int temp;
	for(int i=0;i<n;i++)
	{
		temp=a[small];
		a[small]=a[large];
		a[large]=temp;
	}
	return 0;
}
int output(int n,int a[n])
{
	printf("The new array=\n");
	for(int i=0;i<n;i++)
	printf("%d\t",a[i]);
	return 0;
}
int main()
{
	int n,small,large;
	n=input();
	int a[n];
	input_array(n,a);
	small=find_small(n,a);
	large=find_large(n,a);
	swap(large,small,n,a);
	output(n,a);
	return 0;
}	
	
	