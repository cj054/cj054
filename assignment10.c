#include<stdio.h>
int a[10][10];
int input_row()
{
	int r;
	printf("Enter the number of rows-");
	scanf("%d",&r);
	return r;
}
int input_column()
{
	int c;
	printf("Enter the number of columns-");
	scanf("%d",&c);
	return c;
}
void input_array(int r,int c)
{
	printf("Enter the matrix elements-");
	for(int i=0;i<r;i++)
	{
		for(int j=0;j<c;j++)
		{
		    scanf("&d",&a[i][j]);
		}    
	}
}
void output(int r,int c)
{
	printf("The matrix-");
	for(int i=0;i<r;i++)
	{
		for(int j=0;j<c;j++)
		{
		    printf("%d ",a[i][j]);
		}    
	printf("\n");
	}
}
int main()
{
	int r,c;
	r=input_row();
	c=input_column();
	input_array(r,c);
	output(r,c);
	return 0;
} 
