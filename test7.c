#include<stdio.h>
struct employee
{
    char name[20];
    char id[10];
    int salary;
};
typedef struct employee emp;
emp input_employee()
{
    emp a;
    printf("Enter the employee details-\n");
    printf("Name:");
    gets(a.name);
    printf("ID:");
    gets(a.id);
    printf("Salary:");
    scanf("%d",&a.salary);
    return a;
}
void output(emp a)
{
    printf("The employee details are-\n");
    printf("Name:");
    puts(a.name);
    printf("ID:");
    puts(a.id);
    printf("Salary:%d",a.salary);
}
int main()
{
    emp a;
    a=input_employee();
    output(a);
    return 0;
}