#include<stdio.h>
int input()
{
	int num;
	printf("Enter a number-");
	scanf("%d",&num);
	return num;
}
int find_sum(int num)
{
	int temp,sum=0,n=num;
	while(n!=0)
	{
		temp=n%10;
		sum=sum+temp;
		n=n/10;
	}
	return sum;
}
int output(int sum,int num)
{
	printf("Sum of the digits in %d=%d\n",num,sum);
	return 0;
}
int main()
{
	int num,sum;
	num=input();
	sum=find_sum(num);
	output(sum,num);
	return 0;
}	