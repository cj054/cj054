#include<stdio.h>
int input()
{
    int n;
    printf("Enter the limit-");
    scanf("%d",&n);
    return n;
}
float compute(int n)
{
    float fact=1,sum=0;
    for(int i=1;i<=n;i++)
    {
        fact=fact*i;
        sum=sum+(i/fact);
    }
    return sum;
}
int output(float sum)
{
    printf("Sum=%f",sum);
    return 0;
}
int main()
{
    int n;
    float sum;
    n=input();
    sum=compute(n);
    output(sum);
    return 0;
}    