#include<stdio.h>
int input()
{
    int n;
    printf("Enter a decimal number-");
    scanf("%d",&n);
    return n;
}
int convert(int n)
{
    int bin=0;
    int rem,i=1,step=1;
    while(n>0) 
    {
        rem=n%2;
        bin=bin+(rem*i);
        i=i*10;
        n=n/2;
    }
    return bin;
}
int output(int bin)
{
    printf("The binary form of this number=%d",bin);
    return 0;
}  
int main()
{
    int n,bin;
    n=input();
    bin=convert(n);
    output(bin);
    return 0;
}  