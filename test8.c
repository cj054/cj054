#include<stdio.h>
void input(int *a,int *b)
{
    printf("Enter the value of a and b-");
    scanf("%d %d",a,b);
}
void output1(int a,int b)
{
    printf("The numbers before swapped-\na=%d,b=%d\n",a,b);
}
void swap(int *x,int *y)
{
    int temp;
    temp=*x;
    *x=*y;
    *y=temp;
}
void output2(int a,int b)
{
    printf("The number after swapped-\na=%d,b=%d\n",a,b);
}
int main()
{
    int a,b;
    input(&a,&b);
    output1(a,b);
    swap(&a,&b);
    output2(a,b);
    return 0;
}