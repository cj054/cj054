#include<stdio.h>
#include<math.h>
int input()
{
	int a;
	printf("Enter a number-");
	scanf("%d",&a);
	return a;
}
int find_roots(int a,int b,int c)
{
	float r,r1,r2,d;
	d=(b*b)-(4*a*c);
	if(d>0)
	{
		r1=(-b)+sqrt(d)/(2*a);
		r2=(-b)-sqrt(d)/(2*a);
		printf("Roots are real\nThe roots=%f and %f\n",r1,r2);
	}
	else if(d==0)
	{
		r=(-b)/(2*a);
		printf("The roots are real and equal\nThe roots=%f\n",r);
	}
	else
	{
		printf("The roots are imaginary\n");
	}
	return 0;
}
int main()
{
	int a,b,c;
	a=input();
	b=input();
	c=input();
	find_roots(a,b,c);
	return 0;
}
	