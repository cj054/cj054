#include<stdio.h>
int input()
{
    int n;
    printf("Enter a number-");
    scanf("%d",&n);
    return n;
}
int compute(int n)
{
    int num=n;
    int rem,new=0;
    while(num>0)
    {
        rem=num%10;
        new=(new*10)+rem;
        num=num/10;
    }
    return new;
}
int output(int new,int n)
{
    if(new==n)
    printf("The given number is a palindrome\n");
    else
    printf("The given number is not a palindrome\n");
    return 0;
}
int main()
{
    int n,new;
    n=input();
    new=compute(n);
    output(new,n);
    return 0;
}
