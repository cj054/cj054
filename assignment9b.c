#include<stdio.h>
int a[100];
int input()
{
    int n;
    printf("Enter the number of elements-");
    scanf("%d",&n);
    return n;
}
void input_array(int n)
{
    printf("Enter the array elements in increasing order-");
    for(int i=0;i<n;i++)
    scanf("%d",&a[i]);
}
int search_ele(int n)
{
    int ele;
    printf("Enter the seach element-");
    scanf("%d",&ele);
    return ele;
}    
int compute(int n,int ele)
{
    int first=0,mid,last=n-1,loc=-1;
    while(first<=last)
    {
        mid=(first+last)/2;
        if(ele==a[mid])
        {
            loc=mid;
            break;
        }
        else if(ele<a[mid])
        last=mid-1;
        else
        first=mid+1;
     }
     return loc; 
}
int output(int loc)
{
    printf("The element was found at %d",loc+1);
    return 0;
}
int main()
{
    int n,ele,loc;
    n=input();
    input_array(n);
    ele=search_ele(n);
    loc=compute(n,ele);
    output(loc);
    return 0;
}    