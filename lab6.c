#include<stdio.h>
int input()
{
	int n;
	printf("Enter the number of elements-");
	scanf("%d",&n);
	return n;
}
int find_sum(int n)
{
	int sum=0;
	for(int i=0;i<=n;i++)
	sum=sum+i;
	return sum;
}
float find_avg(int sum,int n)
{
	float avg=(float)sum/n;
	return avg;
}
int output(float avg)
{
	printf("Average of the elements=%f\n",avg);
	return 0;
}	
int main()
{
	int n,sum;
	float avg;
	n=input();
	sum=find_sum(n);
	avg=find_avg(sum,n);
	output(avg);
	return 0;
}	