#include<stdio.h>
int input(int *s,int *m)
{
    printf("Enter the number of students and subjects-");
    scanf("%d %d",s,m);
}    
void input_matrix(int s,int m,int a[s][m])
{
    int i,j;
    for(i=0;i<s;i++)
    {
        printf("Enter the %d student marks-",i+1);
        for(j=0;j<m;j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
        
}    
void find_highest(int s,int m,int a[s][m],int high[m])
{
    int i,j;
    for(j=0;j<m;j++)
    {
        high[j]=a[0][j];
        for(i=1;i<s;i++)
        {
            if(high[j]<a[i][j])
            high[j]=a[i][j];
        }
    } 
}
void output_marks(int s,int m,int a[s][m])
{
    int i,j;
    printf("The marks are-\n");
    for(i=0;i<s;i++)
    {
        for(j=0;j<m;j++)
        {
            printf("%d\t",a[i][j]);    
        }
        printf("\n");
    }
}    
void output_high_marks(int s,int m,int a[s][m],int high[m])
{
    for(int i=0;i<m;i++)
    {
        printf("The highest marks in %d subject=%d\n",i+1,high[i]);
    }    
}
int main()
{
    int s,m;
    input(&s,&m);
    int a[s][m],high[m];
    input_matrix(s,m,a);
    find_highest(s,m,a,high);
    output_marks(s,m,a);
    output_high_marks(s,m,a,high);
    return 0;
} 