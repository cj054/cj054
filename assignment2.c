#include<stdio.h>
float input()
{
  float r;
  printf("Enter the radius-");
  scanf("%f",&r);
  return r;
}
float compute(float r)
{
  float pi=3.14;
  float area=pi*r*r;
  return area;
}
int output(float area)
{
  printf("The area=%f",area);
  return 0;
}  
int main()
{
  float x,a1;
  x=input();
  a1=compute(x);
  output(a1);
  return 0;
 }
