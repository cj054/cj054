#include<stdio.h>
#include<math.h>
struct roots
{
    float real1;
    float real2;
    float img;
}; 
typedef struct roots root;
int input_a()
{
	int a;
	printf("Enter the value of a=");
	scanf("%d",&a);
	return a;
}
int input_b()
{
	int b;
	printf("Enter the value of b=");
	scanf("%d",&b);
	return b;
}
int input_c()
{
	int c;
	printf("Enter the value of c=");
	scanf("%d",&c);
	return c;
}
root find_roots(int a,int b,int c)
{
	float d=(b*b)-(4*a*c);
	root r;
	if(d>0)
	{
		r.real1=(-b)+sqrt(d)/(2*a);
		r.real2=(-b)-sqrt(d)/(2*a);
		return r;
	}
	else if(d==0)
	{
		r.real1=(-b)/(2*a);
		return r;
	}
	else
	{
	    r.img=sqrt(-d);
		r.real1=(-b)/(2*a);
		return r;
	}
}
int find_check(int a,int b,int c)
{
    float d=(b*b)-(4*a*c);
    if(d>0)
    return 1;
    else if(d==0)
    return 2;
    else 
    return 3;
}
void output(root r,int check)
{
    if(check==1)
    printf("Roots are real\nRoots=%f and %f",r.real1,r.real2);
    else if(check==2)
    printf("Roots are real and equal\nRoot=%f",r.real1);
    else
    printf("Roots are imaginary\nRoots=%f+%fi and %f-%fi",r.real1,r.img,r.real1,r.img);
}
int main()
{
	int a,b,c,check;
	root r;
	a=input_a();
	b=input_b();
	c=input_c();
	r=find_roots(a,b,c);
	check=find_check(a,b,c);
	output(r,check);
	return 0;
}