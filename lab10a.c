#include<stdio.h>
void choice(int *c)
{
    printf("1.Addition\n2.Subtraction\n3.Multiplication\n4.Division\n5.Remainder\n");
    printf("Enter the serial number for the arithmetic operations-");
    scanf("%d",c);
}
void input(int *n1,int *n2)
{
    printf("Enter two numbers for the given arithmetic operations-");
    scanf("%d %d",n1,n2);
}
int sum(int *x,int *y)
{
    int add=*x+*y;
    return add;
}
int diff(int *x,int *y)
{
    int sub=*x-*y;
    return sub;
}
int product(int *x,int *y)
{
    int multi=*x * *y;
    return multi;
}
float division(int *x,int *y)
{
    float quo=(float)*x / *y;
    return quo;
}
int reminder(int *x,int *y)
{
    int rem=*x%*y;
    return rem;
}
void output1(int *x,int *y)
{
    printf("The sum of %d+%d=%d\n",*x,*y,sum(x,y));
}
void output2(int *x,int *y)
{
    printf("The difference of %d-%d=%d\n",*x,*y,diff(x,y));
}
void output3(int *x,int *y)
{
    printf("The product of %d*%d=%d\n",*x,*y,product(x,y));
}
void output4(int *x,int *y)
{
    printf("The quotient of %d/%d=%f\n",*x,*y,division(x,y));
}
void output5(int *x,int *y)
{
    printf("The reminder of %d/%d=%d\n",*x,*y,reminder(x,y));
}
void output_error()
{
    printf("Wrong input\n");
}
int main()
{
    int c,n1,n2;
    choice(&c);
    if(c==1)
    {
        input(&n1,&n2);
        output1(&n1,&n2);
    }
    else if(c==2)
    {
        input(&n1,&n2);
        output2(&n1,&n2);
    }
    else if(c==3)
    {
        input(&n1,&n2);
        output3(&n1,&n2);
    }
    else if(c==4)
    {
        input(&n1,&n2);
        output4(&n1,&n2);
    }
    else if(c==5)
    {
        input(&n1,&n2);
        output5(&n1,&n2);
    }
    else
    {
        output_error();
    }
    return 0;
}