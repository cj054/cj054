#include<stdio.h>
#include<math.h>
int input()
{
    int n;
    printf("Enter the limit-");
    scanf("%d",&n);
    return n;
}
float compute(int n)
{
    float sum=0;
    for(float i=1;i<=n;i=i+2)
    {
        sum=sum+(pow(i,2)/pow(i,3));
    }
    return sum;
}
int output(float sum)
{
    printf("Sum=%f",sum);
    return 0;
}
int main()
{
    int n;
    float sum;
    n=input();
    sum=compute(n);
    output(sum);
    return 0;
}    