#include<stdio.h>
char input()
{
	char ch;
	printf("Enter a character-");
	scanf("%c",&ch);
	return ch;
}
int check_char(char ch)
{
	if((ch>=48)&&(ch<=57))
    {
        printf("%c is a digit\n",ch);
    }
    else if ((ch>=97)&&(ch<=122))
    {
        printf("%c is a lower case alphabet\n",ch);
    }
    else if ((ch>=65)&&(ch<=90))
    {
        printf("%c is a upper case alphabet\n",ch);
    }
	else if(ch==32)
	{
		printf("%c is a space\n",ch);
	}	
    else
    {
        printf("%c is a special character\n",ch);
    }
	return 0;
}
int main()
{
	char ch;
	ch=input();
	check_char(ch);
	return 0;
}