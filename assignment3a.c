#include<stdio.h>
#include<math.h>
void input_point(float *x, float *y)
{
	printf("Enter the coordinates of the point-");
	scanf("%f %f",x,y);
}
float compute(float x1,float y1,float x2,float y2)
{
	float d=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
	return d;
}
int output(float d)
{
	printf("The distance between two points=%f",d);
	return 0;
}	
int main()
{
float x1,y1,x2,y2,d1;
input_point(&x1,&y1);
input_point(&x2,&y2);
d1=compute(x1,y1,x2,y2);
output(d1);
return 0;
}
