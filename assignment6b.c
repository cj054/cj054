#include<stdio.h>
int input()
{
    int num;
    printf("Enter a number-");
    scanf("%d",&num);
    return num;
}
int compute(int num)
{
    int count=0;
    for(int i=1;i<=num;i++)
    {
        if(num%i==0)
        count++;
    }
    return count;
}
int output(int count)
{
    if(count==2)
    printf("The number entered is a prime number\n");
    else 
    printf("The number is a composite number\n");
    return 0;
}
int main()
{
    int num,count;
    num=input();
    count=compute(num);
    output(count);
    return 0;
}  